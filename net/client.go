package net

import (
	"encoding/binary"
	"fmt"
	"net"
	"time"
)

type Client struct {
	Network string
	Addr    string
}

func NewClient(network, addr string) *Client {
	return &Client{
		Network: network,
		Addr:    addr,
	}
}

func (c *Client) Send(data string) (err error) {
	conn, err := net.DialTimeout(c.Network, c.Addr, 5*time.Second)
	if err != nil {
		return
	}
	length := len(data)

	B8 := make([]byte, length+NumOfByte)

	binary.BigEndian.PutUint64(B8[:NumOfByte], uint64(length))
	copy(B8[NumOfByte:], []byte(data))

	_, err = conn.Write(B8)
	if err != nil {
		return
	}
	B16 := make([]byte, 2*(length+NumOfByte))
	_, err = conn.Read(B16)
	if err != nil {
		return
	}
	fmt.Println(string(B16))

	return

}
