/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-10-09 11:57:00
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-10-09 12:11:18
 * @FilePath: \go-course\net\server.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package net

import (
	"encoding/binary"
	"net"
)

const (
	NumOfByte = 8
)

type Server struct {
	NetWork string
	Addr    string
}

func NewServer(network, addr string) *Server {
	return &Server{
		NetWork: network,
		Addr:    addr,
	}
}

func (s *Server) Send() (err error) {
	listener, err := net.Listen(s.NetWork, s.Addr)
	if err != nil {
		return
	}

	for {
		conn, err := listener.Accept()
		if err != nil {
			return err
		}
		go s.ConnHandle(conn)
	}
}

func (s *Server) ConnHandle(conn net.Conn) (err error) {
	defer conn.Close()
	B8 := make([]byte, NumOfByte)
	_, err = conn.Read(B8)
	if err != nil {
		return
	}

	length := binary.BigEndian.Uint64(B8)

	resBs := make([]byte, length)
	_, err = conn.Read(resBs)
	if err != nil {
		return
	}

	res := double(resBs)
	_, err = conn.Write(res)
	if err != nil{
		return
	}

	return
}

func double(B8 []byte) []byte {
	B16 := make([]byte, 2*len(B8))
	copy(B16[:len(B8)], B8)
	copy(B16[len(B8):], B8)
	return B16
}
