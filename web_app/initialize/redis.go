/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-29 17:28:59
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-30 08:21:30
 * @FilePath: \go-course\web_app\initialize\redis.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package initialize

import (
	"context"
	"fmt"
	"time"

	"gitee.com/mq-go/go-course/web_app/global"
	"github.com/redis/go-redis/v9"
)

func InitRedis() {
	var redisConfig = global.WebConfig.RedisConfig
	global.RDB = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", redisConfig.Host, redisConfig.Port),
		Password: redisConfig.PassWord,
		DB:       redisConfig.DB,
	})
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_, err := global.RDB.Ping(ctx).Result()
	if err != nil {
		panic(err)
	}
}
