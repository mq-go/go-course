/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-29 17:28:33
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-29 19:40:08
 * @FilePath: \go-course\web_app\initialize\mysql.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package initialize

import (
	"fmt"
	"log"
	"os"
	"time"

	"gitee.com/mq-go/go-course/web_app/global"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
)

func InitMysql() {
	//dsn := "root:root@tcp(192.168.12.100:3306)/web_demo?charset=utf8mb4&parseTime=True&loc=Local"
	var mysqlConfig = global.WebConfig.MysqlConfig
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", mysqlConfig.User,
		mysqlConfig.PassWord, mysqlConfig.Host, mysqlConfig.Port, mysqlConfig.DBName)
	newLogger := logger.New(
		log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer
		logger.Config{
			SlowThreshold:             time.Second, // 缓慢的 SQL 阈值
			LogLevel:                  logger.Info, // 日志级别
			IgnoreRecordNotFoundError: true,        // 忽略日志记录器 ErrRecordNotFound 错误
			ParameterizedQueries:      true,        // 不要在 SQL 日志中包含参数
			Colorful:                  true,        // 禁用颜色
		},
	)
	// Globally mode
	var err error
	global.DB, err = gorm.Open(mysql.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			SingularTable: true,
		},
		Logger: newLogger,
	})
	if err != nil {
		panic(global.DB.Error.Error())
	}

}
