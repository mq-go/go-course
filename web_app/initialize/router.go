/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-29 17:30:01
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-29 17:59:44
 * @FilePath: \go-course\web_app\initialize\router.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package initialize

import (
	"net/http"

	"gitee.com/mq-go/go-course/web_app/global"
	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	Router := gin.New()
	Router.Use(GinLogger(GinLog), GinRecovery(GinLog, true))

	Router.GET("/test", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, global.WebConfig.MysqlConfig.DBName)
	})

	return Router
}
