/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-29 17:33:15
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-29 20:00:34
 * @FilePath: \go-course\web_app\global\global.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package global

import (
	"gitee.com/mq-go/go-course/web_app/settings"
	"github.com/redis/go-redis/v9"
	"gorm.io/gorm"
)

var (
	DB        *gorm.DB
	RDB       *redis.Client
	WebConfig *settings.WebConfig = &settings.WebConfig{}
)
