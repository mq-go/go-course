/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-29 20:07:35
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-29 20:51:01
 * @FilePath: \go-course\web_app\test\config_test.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-29 20:07:35
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-29 20:13:16
 * @FilePath: \go-course\web_app\test\config_test.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package test

import (
	"fmt"
	"testing"

	"gitee.com/mq-go/go-course/web_app/global"
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

func TestInitConfig(t *testing.T) {
	v := viper.New()
	v.SetConfigFile("../config.yaml")
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	fmt.Print(global.WebConfig.MysqlConfig.DBName)
	fmt.Print(global.WebConfig.MysqlConfig.Host)
	fmt.Print(global.WebConfig.MysqlConfig.PassWord)
	fmt.Print(global.WebConfig.MysqlConfig.Port)
	fmt.Print(global.WebConfig.MysqlConfig.User)
	//监听yaml文件
	v.WatchConfig()
	v.OnConfigChange(func(e fsnotify.Event) {
		zap.S().Infof("【配置信息产生变化】 %s", e.Name)
		zap.L().Sugar().Infof("【配置信息产生变化】 %s", e.Name)
		_ = v.ReadInConfig()
		_ = v.Unmarshal(global.WebConfig)
		zap.S().Infof("【配置文件信息】 改为 %v", global.WebConfig)
		zap.L().Sugar().Infof("【配置文件信息】 改为 %v", global.WebConfig)
	})

}
