/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-10-08 13:45:50
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-10-08 14:06:48
 * @FilePath: \go-course\TaskPool\task_pool.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package TaskPool

import "sync/atomic"

type Task func()

type TaskPool struct {
	tasks chan Task
	close *atomic.Bool
}

// NewTaskPool创建任务池 numG 是goroutine数量 capacity是缓存的容量
func NewTaskPool(numG int, capacity int) *TaskPool {
	res := &TaskPool{
		tasks: make(chan Task, capacity),
		close: &atomic.Bool{},
	}
	res.close.Store(false)

	for i:=0; i < numG; i++{
		go func() {
			for t := range res.tasks{
				if res.close.Load() {
					return
				}
				t()
			}
		}()
	}

	return res
}

func (p *TaskPool)Submit(t Task){
	p.tasks <- t
}

func (p *TaskPool)Close() error {
	p.close.Store(true)
	return nil
}
