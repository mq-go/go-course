/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-10-08 14:06:48
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-10-09 12:06:17
 * @FilePath: \go-course\TaskPool\task_pool2.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package TaskPool

type Task2 func()

type TaskPool2 struct {
	Tasks chan Task2
	Close chan struct{}
}

func NewTaskPool2(numG int, capacity int) *TaskPool2 {
	res := &TaskPool2{
		Tasks: make(chan Task2, capacity),
		Close: make(chan struct{}),
	}

	for i := 0; i < numG; i++ {
		go func() {
			for {
				select {
				case <-res.Close:
					return
				case t := <-res.Tasks:
					t()
				}
			}
		}()
	}

	return res
}

func (p *TaskPool2) Submit2(t Task2) {
	p.Tasks <- t
}

func (p *TaskPool2) Close2() error {
	p.Close <- struct{}{}
	return nil
}
