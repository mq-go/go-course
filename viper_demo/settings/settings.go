/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-29 20:16:32
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-30 08:40:00
 * @FilePath: \go-course\viper_demo\settings\settings.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package settings

type WebConfig struct {
	AppConfig   AppConfig   `mapstructure:"app" json:"app" yaml:"app"`
	LogConfig   LogConfig   `mapstructure:"log" json:"log" yaml:"log"`
	MysqlConfig MysqlConfig `mapstructure:"mysql" json:"mysql" yaml:"mysql"`
	RedisConfig RedisConfig `mapstructure:"redis" json:"redis" yaml:"redis"`
}

type AppConfig struct {
	Name string `mapstructure:"name" json:"name" yaml:"name"`
	Mode string `mapstructure:"mode" json:"mode" yaml:"mode"`
	Port int    `mapstructure:"port" json:"port" yaml:"port"`
}

type LogConfig struct {
	Level       string `mapstructure:"level" json:"level" yaml:"level"`
	FileName    string `mapstructure:"filename" json:"filename" yaml:"filename"`
	GinFileName string `mapstructure:"ginFilename" json:"ginFilename" yaml:"ginFilename"`
	MaxSize     int    `mapstructure:"max_size" json:"max_size" yaml:"max_size"`
	MaxAge      int    `mapstructure:"mode" json:"mode" yaml:"mode"`
	MaxBackUps  int    `mapstructure:"max_age" json:"max_age" yaml:"max_age"`
}

type MysqlConfig struct {
	Host     string `mapstructure:"host" json:"host" yaml:"host"`
	Port     int    `mapstructure:"port" json:"port" yaml:"port"`
	User     string `mapstructure:"user" json:"user" yaml:"user"`
	PassWord string `mapstructure:"password" json:"password" yaml:"password"`
	DBName   string `mapstructure:"dbname" json:"dbname" yaml:"dbname"`
}

type RedisConfig struct {
	Host     string `mapstructure:"host" json:"host" yaml:"host"`
	Port     int    `mapstructure:"port" json:"port" yaml:"port"`
	PassWord string `mapstructure:"password" json:"password" yaml:"password"`
	DB       int    `mapstructure:"db" json:"db" yaml:"db"`
}
