/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-29 20:15:41
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-30 08:42:24
 * @FilePath: \go-course\viper_demo\main.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package main

import (
	"fmt"

	"gitee.com/mq-go/go-course/viper_demo/global"
	"github.com/spf13/viper"
)

func main() {
	v := viper.New()
	v.SetConfigFile("config.yaml")
	err := v.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s \n", err))
	}
	err = v.Unmarshal(global.WebConfig)
	if err != nil {
		panic(err)
	}

	var redisConfig = global.WebConfig.RedisConfig
	var mysqlConfig = global.WebConfig.MysqlConfig
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", mysqlConfig.User,
		mysqlConfig.PassWord, mysqlConfig.Host, mysqlConfig.Port, mysqlConfig.DBName)
	redis := fmt.Sprintf("%s:%d", global.WebConfig.RedisConfig.Host, redisConfig.Port)
	fmt.Println(redis)
	fmt.Println(dsn)
	fmt.Println(global.WebConfig.MysqlConfig.DBName)
	fmt.Println(mysqlConfig.Host)
	fmt.Println(global.WebConfig.MysqlConfig.PassWord)
	fmt.Println(global.WebConfig.MysqlConfig.Port)
	fmt.Println(global.WebConfig.MysqlConfig.User)

	// fmt.Print(global.AppConfig.MysqlConfig.DBName)
	// fmt.Print(global.AppConfig.MysqlConfig.Host)
	// fmt.Print(global.AppConfig.MysqlConfig.PassWord)
	// fmt.Print(global.AppConfig.MysqlConfig.Port)
	// fmt.Print(global.AppConfig.MysqlConfig.User)
}
