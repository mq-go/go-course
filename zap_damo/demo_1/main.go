/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-09-28 20:26:45
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-09-29 13:52:04
 * @FilePath: \classoned:\Data\Gitee\go-course\zap_damo\main.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package main

import (
	"net/http"

	_ "github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

var logger *zap.Logger

func main() {
	InitLogger()
	defer logger.Sync()
	simpleHttpGet("www.google.com")
	simpleHttpGet("http://www.google.com")

	// router := gin.Default()
	// zapGroup := router.Group("/zap")
	// zapGroup.GET("/hello", func(ctx *gin.Context) {
	// 	ctx.JSON(200, gin.H{
	// 		"message": "hello mengqi",
	// 	})
	// })
	// router.Run()
}

// zap
func InitLogger() {
	//logger, _ = zap.NewProduction()
	logger, _ = zap.NewDevelopment()
}

func simpleHttpGet(url string) {
	resp, err := http.Get(url)
	if err != nil {
		logger.Error(
			"Error fetching url..",
			zap.String("url", url),
			zap.Error(err))
	} else {
		logger.Info("Success..",
			zap.String("statusCode", resp.Status),
			zap.String("url", url))
		resp.Body.Close()
	}
}
