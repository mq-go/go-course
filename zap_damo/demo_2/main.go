package main

import (
	"net/http"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

// 定制log
var sugarLogger *zap.SugaredLogger

func main() {

	InitLogger()
	defer sugarLogger.Sync()
	//simpleHttpGet("www.google.com")
	//simpleHttpGet("http://www.google.com")
	for i := 0; i < 1000; i++ {
		sugarLogger.Info("测试切割")
		sugarLogger.Info("测试切割")
	}

}

// func getLogWriter() zapcore.WriteSyncer {
// 	file, _ := os.OpenFile("../logs/test.log", os.O_CREATE|os.O_APPEND|os.O_RDWR, 0744)
// 	return zapcore.AddSync(file)
// }

func getLogWriter() zapcore.WriteSyncer {
	lumberJackLogger := &lumberjack.Logger{
		Filename:   "../logs/test.log",
		MaxSize:    1,     //M
		MaxBackups: 2,     //备份数量
		MaxAge:     30,    //最大备份天数
		Compress:   false, //压缩
	}
	return zapcore.AddSync(lumberJackLogger)
}

func getEncoder() zapcore.Encoder {
	encodingCofig := zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		FunctionKey:    zapcore.OmitKey,
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder,
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	return zapcore.NewJSONEncoder(encodingCofig)
}

func InitLogger() {
	writeSyncer := getLogWriter()
	encoder := getEncoder()
	core := zapcore.NewCore(encoder, writeSyncer, zapcore.DebugLevel)

	logger := zap.New(core, zap.AddCaller())
	sugarLogger = logger.Sugar()
}

func simpleHttpGet(url string) {
	resp, err := http.Get(url)
	if err != nil {
		sugarLogger.Error(
			"Error fetching url..",
			zap.String("url", url),
			zap.Error(err))
	} else {
		sugarLogger.Info("Success..",
			zap.String("statusCode", resp.Status),
			zap.String("url", url))
		resp.Body.Close()
	}
}
