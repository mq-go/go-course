/*
 * @Author: drowningwhale 351231768@qq.com
 * @Date: 2023-10-09 16:45:31
 * @LastEditors: drowningwhale 351231768@qq.com
 * @LastEditTime: 2023-10-09 17:41:15
 * @FilePath: \go-course\Pool\NewPool.go
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
package pool

import (
	"context"
	"errors"
	"net"
	"sync"
	"time"
)

type newPool struct {
	//空闲连接队列
	idlesConns chan *idleConn

	//等待队列
	reqQueue chan net.Conn

	//连接数
	cnt int

	//最大连接数
	maxCnt int

	//最大空闲连接数
	maxIdleCnt int

	//最大空闲时间
	maxIdleTime time.Duration

	//方法
	factory func() (net.Conn, error)

	//锁
	lock sync.Mutex
}

type idleConn struct {
	conn           net.Conn
	lastActiveTime time.Time
}

func NewNewPool(initCnt, maxCnt, maxIdleCnt int, maxIdleTime time.Duration, factory func() (net.Conn, error)) (*newPool, error) {
	if initCnt > maxCnt {
		return nil, errors.New("初始化连接数大于最大连接数")
	}

	idlesConns := make(chan *idleConn, maxIdleCnt)
	for i := 0; i < initCnt; i++ {
		conn, err := factory()
		if err != nil {
			return nil, err
		}
		idlesConns <- &idleConn{
			conn:           conn,
			lastActiveTime: time.Now(),
		}

	}

	res := &newPool{
		idlesConns:  idlesConns,
		reqQueue:    make(chan net.Conn, maxCnt),
		cnt:         0,
		maxCnt:      maxCnt,
		maxIdleCnt:  maxIdleCnt,
		maxIdleTime: maxIdleTime,
		factory:     factory,
	}
	return res, nil
}

func (p *newPool) Get(ctx context.Context) (net.Conn, error) {
	//超时控制
	select {
	case <-ctx.Done():
		return nil, ctx.Err()
	default:
	}
	//有没有空闲连接
	for {
		select {
		//有空闲连接
		case idleConn := <-p.idlesConns:
			//判断是否超时 超时则重新获取连接
			if idleConn.lastActiveTime.Add(p.maxIdleTime).Before(time.Now()) {
				_ = idleConn.conn.Close()
				p.cnt--
				continue
			}
			//没有超时则返回连接
			return idleConn.conn, nil
		//没有空闲连接 查看是否超过最大数量  没超过创建新连接  超过加入等待队列
		default:
			p.lock.Lock()

			if p.cnt > p.maxCnt {
				p.lock.Unlock()
				//超过最大数量
				select {
				//向等待队列请求连接
				//判断超时
				case <-ctx.Done():
					//超时直接返回
					return nil, ctx.Err()
				//没满
				case c := <-p.reqQueue:
					return c, nil
				//满了
				default:
					continue
				}
			}
			//没超过最大数量
			defer p.lock.Unlock()
			conn, err := p.factory()
			if err != nil {
				return nil, err
			}
			p.cnt++
			return conn, nil

		}

	}
}

func (p *newPool) Put(conn net.Conn) error {
	//等待队列有没有在等待的
	select {
	//有等待的
	case p.reqQueue <- conn:
		return nil
	//没有等待的
	default:
		//放入空闲连接队列
		//判断空闲连接队列满没满
		idleConn := &idleConn{
			conn:           conn,
			lastActiveTime: time.Now(),
		}
		select {
		//如果空闲队列没满
		case p.idlesConns <- idleConn:
		//空闲队列满了
		default:
			//空闲队列满了
			conn.Close()
			//连接数减一
			p.lock.Lock()
			p.cnt--
			p.lock.Unlock()

		}
		return nil
	}
}

func (p *newPool) Close() {
	close(p.idlesConns)
	close(p.reqQueue)
}
