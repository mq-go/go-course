package pool

import (
	"context"
	"errors"
	"net"
	"sync"
	"time"
)

type Pool struct {
	//空闲链接队列
	idlesConns chan *idlesConn
	//请求队列
	reqQueue []connReq
	//连接数
	cnt int
	//最大连接数
	maxCnt int
	//最大等待数量
	maxIdleCnt int
	//最大空闲时间
	maxIdleTime time.Duration
	//创建连接方法
	factory func() (net.Conn, error)
	//锁
	lock sync.Mutex
}

type idlesConn struct {
	conn           net.Conn
	lastActiveTime time.Time
}

type connReq struct {
	connChan chan net.Conn
}

func NewPool(initCnt, maxIdleCnt, maxCnt int, maxIdleTime time.Duration, factory func() (net.Conn, error)) (*Pool, error) {
	//校验参数
	if initCnt > maxIdleCnt {
		return nil, errors.New("初始化连接数大于最大等待连接数")
	}

	//初始化连接
	idlesConns := make(chan *idlesConn, maxIdleCnt)
	for i := 0; i < initCnt; i++ {
		conn, err := factory()
		if err != nil {
			return nil, err
		}
		idlesConns <- &idlesConn{
			conn:           conn,
			lastActiveTime: time.Now(),
		}
	}

	res := &Pool{
		idlesConns:  idlesConns,
		maxCnt:      maxCnt,
		maxIdleCnt:  maxIdleCnt,
		cnt:         0,
		maxIdleTime: maxIdleTime,
		factory:     factory,
	}
	return res, nil
}

// 实现取连接
func (p *Pool) Get(ctx context.Context) (net.Conn, error) {
	select {
	//超时控制
	case <-ctx.Done():
		return nil, ctx.Err()
	default:

	}
	//判断是否有空闲连接
	for {
		//如果连接超时 重新获取连接
		select {
		//有空闲的连接
		case idlesConn := <-p.idlesConns:
			//判断有没有超时
			if idlesConn.lastActiveTime.Add(p.maxIdleTime).Before(time.Now()) {
				//如果获取的连接超时则关闭进入下一次循环继续获取连接
				_ = idlesConn.conn.Close()
				continue
			}
			//如果没有超时返回连接
			return idlesConn.conn, nil
		//没有空闲连接
		default:
			//判断超没超过最大数量
			p.lock.Lock()
			if p.cnt > p.maxCnt {
				//超过最大数量
				//放入等待队列
				req := connReq{make(chan net.Conn, 1)}
				p.reqQueue = append(p.reqQueue, req)
				//在阻塞之前解锁
				p.lock.Unlock()
				select {
				case <-ctx.Done():
					//超时
					//选项1 从队列里删掉
					//选项2 转发
					//实现转发已经获得的连接
					go func() {
						c := <-req.connChan
						_ = p.Put(context.Background(), c)
					}()
					return nil, ctx.Err()
				//等待别人归还连接
				case c := <-req.connChan:
					return c, nil
				}
			}
			//没超过最大数量  创建连接
			//连接数加一
			p.cnt = p.cnt + 1
			conn, err := p.factory()
			p.lock.Unlock()
			if err != nil {
				//创建连接失败
				return nil, err
			}
			return conn, nil
		}
	}

}

func (p *Pool) Put(ctx context.Context, conn net.Conn) error {
	//超时控制
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}
	//有没有阻塞的请求
	p.lock.Lock()
	if len(p.reqQueue) > 0 {
		//有阻塞的请求 直接从队首拿出来
		req := p.reqQueue[0]
		p.reqQueue = p.reqQueue[1:]
		p.lock.Unlock()
		req.connChan <- conn
		return nil
	}
	//没有阻塞的请求放入等待连接队列
	//放入之前查看等待队列满没满
	p.lock.Unlock()
	idlesConn := &idlesConn{
		conn:           conn,
		lastActiveTime: time.Now(),
	}

	select {
	case p.idlesConns <- idlesConn:
	default:
		//空闲队列满了
		conn.Close()
		//连接数减一
		p.lock.Lock()
		p.cnt--
		p.lock.Unlock()
	}
	return nil

}

func (p *Pool) Close() {
	close(p.idlesConns)
}
